#!/usr/bin/python

# Copyright: (c) 2022, Nick Wilburn <senior.crepe@gmail.com>
# GNU General Public License v3.0+
# (see COPYING or https://www.gnu.org/licenses/gpl-3.0.txt)
from __future__ import absolute_import, division, print_function

__metaclass__ = type

import os

import xml.etree.ElementTree as ET

from ansible.module_utils.basic import AnsibleModule


DOCUMENTATION = r'''
---
module: oscap_oval_scan

short_description: Represents an oscap client oval scan with results

version_added: "0.0.1"

description: Scan a target with an oval file

options:
    oval_definition_file:
        description: Specify a path to an OVAL definition xml file
                     on the target host.
            - mutually_exclusive with from_results
            - requires oscap on the path or oscap_bin
        required: false
        type: path
    from_results:
        description: Pass in SCAP oval scan results xml as a
                     string or path to xml on target
            - mutually_exclusive with oval_definition_file
        type: str|path
        required: false
    results_file:
        description: Save the OVAL results xml to a file on the target
        type: path
        required: false
    severity_treshold:
        description: The minimum severity of a result to report on
        type: str
        required: false
        default: medium
    oscap_bin:
        description: Specify the oscap bin path
            - only used with checklist
        type: path
        required: false
    oscap_eval_args:
        description: Specify oscap binary command operations
            -  default ['oval', 'eval']
        type: list
        required: false

author:
    - Nick Wilburn <senior.crepe@gmail.com>
'''

EXAMPLES = r"""
- name: Download Latest Oval file
  get_url:
    url: "https://security-metadata.canonical.com/oval/com.ubuntu.{{ ansible_distribution_release }}.usn.oval.xml.bz2"
    dest: /tmp/scan_oval.bz2

- name: Uncompress the oval file
  shell: |
    bunzip2 -f /tmp/scan_oval.bz2

- name: Run a scan
  oscap_oval_scan:
    oval_definition_file: "/tmp/scan_oval"
    results_file: /tmp/results.xml
"""

RETURN = """
msg:
    description: Text messages about the internal state and mode of this module
    type: str
    returned: always
    sample: this is a string that is all
failed_scan_items:
    description: A list of scan items that failed
    type: list
    returned: always
    sample: [
        {"reference": "https://ubuntu.com/security/notices/USN-5384-1", "severity": "medium", "title": "title"}
    ]
"""


def parse_scan_results(result_xml, severity_threshold):
    ns_map = {
        "oval-definitions": "http://oval.mitre.org/XMLSchema/oval-definitions-5",
        "oval-results": "http://oval.mitre.org/XMLSchema/oval-results-5",
    }

    severity_map = {"none": 0, "low": 1, "medium": 2, "high": 3}

    root = ET.fromstring(result_xml)

    results = []

    for result_definition in root.findall(".//oval-results:definition", ns_map):
        if result_definition.get("result") == "true":
            result_entry = {}
            for oval_definition in root.findall(".//oval-definitions:definition", ns_map):
                if (
                    oval_definition.get("id") == result_definition.get("definition_id")
                    and oval_definition.get("class") != "inventory"
                ):
                    result_severity = oval_definition.find(
                        ".//oval-definitions:severity", ns_map
                    ).text.lower()
                    if severity_map[result_severity] >= severity_map[severity_threshold.lower()]:
                        title = oval_definition.find(".//oval-definitions:title", ns_map).text
                        result_entry["title"] = title
                        result_entry["severity"] = result_severity
                        for reference in oval_definition.findall(
                            ".//oval-definitions:reference", ns_map
                        ):
                            if reference.get("source") == "USN":
                                result_entry["reference"] = reference.get("ref_url")

                        results.append(result_entry)

    return results


def run_module():  # noqa: C901

    module = AnsibleModule(
        dict(
            oval_definition_file=dict(type="path", required=False),
            from_results=dict(type="path", required=False),
            results_file=dict(type="path", required=False),
            severity_threshold=dict(type=str, required=False, default="medium"),
            oscap_bin=dict(type="path", required=False),
            oscap_eval_args=dict(type="list", required=False, default=["oval", "eval"]),
        ),
        required_one_of=[["oval_definition_file", "from_results"]],
        mutually_exclusive=[["oval_definition_file", "from_results"]],
        supports_check_mode=False,
    )
    messages = []
    changed = False

    if module.params["oval_definition_file"]:
        # execute a checklist scan and save results for processing
        if module.params["oscap_bin"]:
            oscap_args = [module.params["oscap_bin"]]
        else:
            oscap_args = [module.get_bin_path("oscap", False)]

        oscap_args.extend(module.params["oscap_eval_args"])
        if module.params["results_file"]:
            out_results = module.params["results_file"]
            changed = True
        else:
            out_results = "-"

        oscap_args.extend(["--results", out_results])
        oscap_args.append(module.params["oval_definition_file"])

        # We should probably check the return code here
        rc, stdout, stderr = module.run_command(oscap_args, check_rc=False)

        if rc == 1:
            messages.append("oscap returned with exit code 1")
            messages.append(stderr)
            module.fail_json(
                msg=", ".join(messages),
                changed=changed,
            )

        if module.params["results_file"]:
            with open(module.params["results_file"]) as outfile:
                from_results = outfile.read()
        else:
            from_results = stdout
            # Strip off the top messages
            from_results = from_results[from_results.find("<?xml"):]
            messages.append("no xml output on oscap stdout")
            if not len(from_results.strip()):
                module.fail_json(
                    msg=", ".join(messages),
                    changed=changed,
                    messages=messages,
                )

    else:
        if os.path.isfile(module.params["from_results"]):
            with open(module.params["from_results"]) as results:
                from_results = results.read()
        else:
            messages.append("from_results does not exist or " "is not valid xml")
            module.fail_json(msg=", ".join(messages), changed=changed, messages=messages)

    results = parse_scan_results(from_results, module.params["severity_threshold"])

    if len(results):
        messages.append("Scan found %s failed checks." % len(results))
        module.fail_json(
            msg=", ".join(messages),
            changed=changed,
            failed_scan_items=results,
        )

    else:
        module.exit_json(changed=changed, messages=messages, failed_scan_items=results)


if __name__ == "__main__":  # pragma: no cover
    run_module()
