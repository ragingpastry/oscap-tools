import json
import pytest
import sys
import os
import shutil

from ansible.module_utils import basic
from ansible.module_utils._text import to_bytes

pth = os.path.realpath(os.path.join(__file__, '..', '..', '..', '..', 'plugins', 'modules'))
sys.path.append(pth)

import oscap_oval_scan  # noqa E402

FIXTURES_PATH = 'tests/unit/oscap_oval_scan/fixtures'


def set_module_args(args, check_mode=False):
    if '_ansible_remote_tmp' not in args:
        args['_ansible_remote_tmp'] = '/tmp'
    if '_ansible_keep_remote_files' not in args:
        args['_ansible_keep_remote_files'] = False

    if check_mode:
        args['_ansible_check_mode'] = True

    args = json.dumps({'ANSIBLE_MODULE_ARGS': args})
    basic._ANSIBLE_ARGS = to_bytes(args)


class AnsibleExitJson(Exception):
    """Exception class to be raised by module.exit_json and caught by the test case"""
    pass


class AnsibleFailJson(Exception):
    pass


def exit_json(self, **kwargs):
    if 'changed' not in kwargs:
        kwargs['changed'] = False
    raise AnsibleExitJson(kwargs)


def fail_json(self, *message, **kwargs):
    if message and not isinstance(message[0], str):
        raise Exception("first param for fail_json must be error message")
    kwargs['failed'] = True
    raise AnsibleFailJson(kwargs)


@pytest.fixture
def oscap_bin_fake():

    bin_dir = os.path.realpath(
        os.path.join(__file__, '..', 'fixtures')
    )
    oscap_src = os.path.join(
        bin_dir, 'oscap_bin_good.sh'
    )
    oscap_tgt = os.path.join(bin_dir, 'oscap')

    os.link(oscap_src, oscap_tgt)

    old_path = os.environ['PATH']
    # Put it infront to make sure we are using our fake bin
    os.environ['PATH'] = bin_dir + ':' + os.environ['PATH']

    yield oscap_tgt

    os.unlink(oscap_tgt)
    os.environ['PATH'] = old_path


def test_normal_args(monkeypatch):
    monkeypatch.setattr(oscap_oval_scan.AnsibleModule, 'exit_json', exit_json)
    monkeypatch.setattr(oscap_oval_scan.AnsibleModule, 'fail_json', fail_json)
    set_module_args({
        'from_results': '{FIXTURES_PATH}/test-oval-scan1.xml'.format(
            FIXTURES_PATH=FIXTURES_PATH)
        }
    )

    with pytest.raises(AnsibleFailJson) as exitception:
        oscap_oval_scan.run_module()

    failed_scan_items = exitception.value.args[0]['failed_scan_items']

    assert ('5384-1 -- Linux kernel vulnerabilities'
            in [item['title'] for item in failed_scan_items])


def test_find_oscap_bin(monkeypatch, oscap_bin_fake):
    monkeypatch.setattr(oscap_oval_scan.AnsibleModule, 'exit_json', exit_json)
    monkeypatch.setattr(oscap_oval_scan.AnsibleModule, 'fail_json', fail_json)
    set_module_args({
        'oval_definition_file': '{FIXTURES_PATH}/com.ubuntu.focal.usn.oval.xml'.format(
            FIXTURES_PATH=FIXTURES_PATH)
        }
    )

    with pytest.raises(AnsibleExitJson):
        oscap_oval_scan.run_module()


@pytest.mark.parametrize('severity,count', [
    ['high', 0],
    ['medium', 1]
])
def test_normal_exits_severity_high(monkeypatch, severity, count):
    monkeypatch.setattr(oscap_oval_scan.AnsibleModule, 'exit_json', exit_json)
    monkeypatch.setattr(oscap_oval_scan.AnsibleModule, 'fail_json', fail_json)
    set_module_args({
        'from_results': '{FIXTURES_PATH}/test-oval-scan1.xml'.format(
            FIXTURES_PATH=FIXTURES_PATH
        ),
        'severity_threshold': severity
        },
    )

    if count > 0:
        with pytest.raises(AnsibleFailJson) as exitception:
            oscap_oval_scan.run_module()
    else:
        with pytest.raises(AnsibleExitJson) as exitception:
            oscap_oval_scan.run_module()

    assert len(exitception.value.args[0]['failed_scan_items']) == count


def test_from_results_w_results_file(monkeypatch, tmp_path):
    monkeypatch.setattr(oscap_oval_scan.AnsibleModule, 'exit_json', exit_json)
    monkeypatch.setattr(oscap_oval_scan.AnsibleModule, 'fail_json', fail_json)

    results_dir = tmp_path / 'test_results_file'
    results_file = os.path.join(str(results_dir), 'tmp_output.xml')

    set_module_args({
        'from_results': 'results_xml_string',
        'results_file': results_file,
        'oval_definition_file': '{FIXTURES_PATH}/com.ubuntu.focal.usb.oval.xml'
        },
    )

    with pytest.raises(AnsibleFailJson):
        oscap_oval_scan.run_module()


def test_exclusive_args_fail(monkeypatch):
    monkeypatch.setattr(oscap_oval_scan.AnsibleModule, 'exit_json', exit_json)
    monkeypatch.setattr(oscap_oval_scan.AnsibleModule, 'fail_json', fail_json)
    set_module_args({
        'oval_definition_file': 'asdfasdf',
        'from_results': 'results_xml_string',
        }
    )

    with pytest.raises(AnsibleFailJson) as failz:
        oscap_oval_scan.run_module()

    assert 'mutually exclusive: oval_definition_file|from_results' in str(failz.value)


def test_invalid_from_results(monkeypatch):
    monkeypatch.setattr(oscap_oval_scan.AnsibleModule, 'fail_json', fail_json)
    set_module_args({
        'from_results': 'invalid_file'
    })

    with pytest.raises(AnsibleFailJson) as fail:
        oscap_oval_scan.run_module()

    assert 'from_results does not exist' in str(fail.value)


@pytest.mark.parametrize('oscap_bin_path,exit_code', [
    ('{FIXTURES_PATH}/oscap_bin_good.sh'.format(
     FIXTURES_PATH=FIXTURES_PATH), 0),
    ('{FIXTURES_PATH}/oscap_bin_error_code.sh'.format(
     FIXTURES_PATH=FIXTURES_PATH), 2),
    ('{FIXTURES_PATH}/oscap_bin_error_code_1.sh'.format(
     FIXTURES_PATH=FIXTURES_PATH), 2),
    ('{FIXTURES_PATH}/oscap_no_bin'.format(
     FIXTURES_PATH=FIXTURES_PATH), 127)
])
def test_oscap_bin_test_paths(monkeypatch, oscap_bin_path, exit_code):
    monkeypatch.setattr(oscap_oval_scan.AnsibleModule, 'exit_json', exit_json)
    monkeypatch.setattr(oscap_oval_scan.AnsibleModule, 'fail_json', fail_json)

    set_module_args({
        'oval_definition_file': '{FIXTURES_PATH}/com.ubuntu.focal.usn.oval.xml'.format(
            FIXTURES_PATH=FIXTURES_PATH),
        'oscap_bin': oscap_bin_path
        }
    )

    if exit_code == 0:
        AnsibleExitClass = AnsibleExitJson
    else:
        AnsibleExitClass = AnsibleFailJson

    with pytest.raises(AnsibleExitClass):
        oscap_oval_scan.run_module()


def test_oscap_out_file(monkeypatch, tmp_path):
    arg_checks = []

    out_results_dir = tmp_path / "out_results"
    out_results_dir.mkdir()

    out_results_path = '{out_results_dir}/results-test.xml'.format(
        out_results_dir=out_results_dir)

    def run_command_checks(self, oscap_args, check_rc=False):
        arg_checks.extend(oscap_args)
        if out_results_path in oscap_args:
            shutil.copy('{FIXTURES_PATH}/test-oval-scan1.xml'.format(
                FIXTURES_PATH=FIXTURES_PATH), out_results_path)
            return 0, 'not valid content, look in {out_results_path}'.format(
                out_results_path=out_results_path), ''
        else:
            with open('{FIXTURES_PATH}/test-oval-scan1.xml'.format(
                      FIXTURES_PATH=FIXTURES_PATH)) as scanfile:
                return 0, scanfile.read(), ''

    monkeypatch.setattr(oscap_oval_scan.AnsibleModule, 'exit_json', exit_json)
    monkeypatch.setattr(oscap_oval_scan.AnsibleModule, 'fail_json', fail_json)
    monkeypatch.setattr(oscap_oval_scan.AnsibleModule, 'run_command', run_command_checks)

    set_module_args({
        'oval_definition_file': '{FIXTURES_PATH}/com.ubuntu.focal.usn.oval.xml'.format(
            FIXTURES_PATH=FIXTURES_PATH),
        'oscap_bin': '{FIXTURES_PATH}/oscap_bin_good.sh'.format(
            FIXTURES_PATH=FIXTURES_PATH),
        'results_file': out_results_path,
        }
    )

    with pytest.raises(AnsibleFailJson):
        oscap_oval_scan.run_module()

    results_actual = out_results_path

    assert results_actual in arg_checks
