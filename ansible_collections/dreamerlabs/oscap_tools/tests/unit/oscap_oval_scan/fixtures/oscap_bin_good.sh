#!/bin/sh

SCRIPTPATH="$(cd "$(dirname "$0")" >/dev/null 2>&1 || exit 1; pwd -P )"

while [ "$1" != "" ]; do
    case $1 in
        --results)
            OUTFILE=$2
            ;;
    esac
    shift
done

if [ "$OUTFILE" = '-' ];
then
  cat ${SCRIPTPATH}/test-oval-scan1-clean.xml
else
  cat ${SCRIPTPATH}/test-oval-scan1-clean.xml > $OUTFILE
fi

exit 0
