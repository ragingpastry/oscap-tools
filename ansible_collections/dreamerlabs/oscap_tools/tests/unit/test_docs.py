import os
import subprocess

pth = os.path.realpath(os.path.join(__file__, '..', '..', '..', 'plugins', 'modules'))


def test_ansible_docs():

    docs = subprocess.check_output(['ansible-doc', '-M', pth, 'oscap_oval_scan'])

    assert 'OSCAP_OVAL_SCAN' in str(docs)
